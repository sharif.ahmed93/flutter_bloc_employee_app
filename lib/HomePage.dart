import 'package:flutter/material.dart';
import 'EmployeeBloc.dart';
import 'Employee.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _employeeBloc = EmployeeBloc();

  @override
  void dispose() {
    _employeeBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Container(
        child: StreamBuilder<List<Employee>>(
          stream: _employeeBloc.employeeListStream,
          builder: (BuildContext context,
              AsyncSnapshot<List<Employee>> asyncSnapshot) {
            return ListView.builder(
              itemCount: asyncSnapshot.data.length,
              itemBuilder: (BuildContext buildContext, position) {
                return Card(
                  elevation: 5.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: Text(
                          "${asyncSnapshot.data[position].id}",
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${asyncSnapshot.data[position].name}",
                              style: TextStyle(fontSize: 18.0),
                            ),
                            Text(
                              "${asyncSnapshot.data[position].salary}",
                              style: TextStyle(fontSize: 16.0),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              icon: Icon(
                                Icons.thumb_up,
                                color: Colors.green,
                              ),
                              onPressed: () {
                                _employeeBloc.employeeSalaryIncrement(
                                    asyncSnapshot.data[position]);
                              },
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.thumb_down,
                                color: Colors.red,
                              ),
                              onPressed: () {
                                _employeeBloc.employeeSalaryDecrement(
                                    asyncSnapshot.data[position]);
                              },
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
