import 'dart:async';
import 'Employee.dart';

class EmployeeBloc {
  List<Employee> _employeeList = [
    Employee(1, "Employee One", 12000.0),
    Employee(2, "Employee Two", 12000.0),
    Employee(3, "Employee Three", 12000.0),
    Employee(4, "Employee Four", 12000.0),
    Employee(5, "Employee Five", 12000.0),
    Employee(6, "Employee Six", 12000.0),
    Employee(7, "Employee Seven", 12000.0),
    Employee(8, "Employee Eight", 12000.0),
    Employee(9, "Employee Nine", 12000.0),
    Employee(10, "Employee Ten", 12000.0),
    Employee(11, "Employee Eleven", 12000.0),
    Employee(12, "Employee Twelve", 12000.0),
    Employee(13, "Employee Thirteen", 12000.0),
    Employee(14, "Employee Fourteen", 12000.0),
    Employee(15, "Employee Fifteen", 12000.0),
    Employee(16, "Employee Sixteen", 12000.0),
    Employee(17, "Employee Seventeen", 12000.0),
  ];


  final _employeeListStreamController = StreamController<List<Employee>>();


  final _employeeSalaryIncrementStreamController = StreamController<Employee>();


  final _employeeSalaryDecrementStreamController = StreamController<Employee>();

  Stream<List<Employee>> get employeeListStream =>
      _employeeListStreamController.stream;

  StreamSink<List<Employee>> get employeeListSink =>
      _employeeListStreamController.sink;

  StreamSink<Employee> get employeeSalaryIncSink =>
      _employeeSalaryIncrementStreamController.sink;

  StreamSink<Employee> get employeeSalaryDecSink =>
      _employeeSalaryDecrementStreamController.sink;

  EmployeeBloc() {
    _employeeListStreamController.add(_employeeList);
    _employeeSalaryIncrementStreamController.stream.listen(employeeSalaryIncrement);
    _employeeSalaryDecrementStreamController.stream.listen(employeeSalaryDecrement);
  }

  void employeeSalaryIncrement(Employee employee) {
    double salary = employee.salary;
    var incrementedSalary = salary * 20 / 100;
    _employeeList[employee.id - 1].salary = salary + incrementedSalary;
    employeeListSink.add(_employeeList);
  }

  void employeeSalaryDecrement(Employee employee) {
    double salary = employee.salary;
    double decrementedSalary = salary * 20 / 100;
    _employeeList[employee.id - 1].salary = salary - decrementedSalary;
    employeeListSink.add(_employeeList);
  }

  void dispose() {
    _employeeSalaryIncrementStreamController.close();
    _employeeSalaryDecrementStreamController.close();
    _employeeListStreamController.close();
  }
}
